<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180423182329 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            '
              INSERT INTO `config` (`id`, `totalPosition`, `totalPlay`) VALUES
              (1, 6, 15);

              INSERT INTO `status` (`id`, `name`) VALUES
              (1, "en juego"),
              (3, "finalizada derrota"),
              (2, "finalizada victoria");

              INSERT INTO `color` (`id`, `name`, `codeHex`, `responseColor`) VALUES
              (1, "negro", "000000", 1),
              (2, "blanco", "FFFFFF", 1),
              (3, "rojo", "FF0000", 1),
              (4, "naranja", "FE9A2E", 1),
              (5, "amarillo", "F7FE2E", 1),
              (6, "verde", "40FF00", 1),
              (7, "azul", "0000FF", 1),
              (8, "rosa", "FA58F4", 1),
              (9, "gris", "848484", 1),
              (10, "turquesa", "00FFFF", 1);
            '
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
