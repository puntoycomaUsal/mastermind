<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180429093712 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            '
            ALTER TABLE `color` CHANGE `responseColor` `blackColor` TINYINT(1) NOT NULL DEFAULT "0";
            ALTER TABLE `color` ADD `whiteColor` BOOLEAN NOT NULL DEFAULT FALSE AFTER `blackColor`, ADD `nullColor` BOOLEAN NOT NULL DEFAULT FALSE AFTER `whiteColor`;
            UPDATE `color` SET `blackColor` = "0";
            UPDATE `color` SET `whiteColor` = "0";
            UPDATE `color` SET `nullColor`  = "0";
            UPDATE `color` SET `blackColor` = "1" WHERE `color`.`id` = 1;
            UPDATE `color` SET `whiteColor` = "1" WHERE `color`.`id` = 2;
            INSERT INTO `color` (`id`, `name`, `codeHex`, `blackColor`, `whiteColor`, `nullColor`) VALUES (NULL, "nulo", "------", "0", "0", "1");
            '
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
