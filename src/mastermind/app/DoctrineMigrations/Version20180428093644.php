<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180428093644 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            '
            ALTER TABLE `game_color` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

            ALTER TABLE `game_color` DROP INDEX `UNIQ_232B318C5EFFFFFF`, ADD UNIQUE `UNIQ_232B318C5EFFFFFF` (`game_id`, `color_id`) USING BTREE;
            
            ALTER TABLE `play_color` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
            
            ALTER TABLE `play_color` DROP INDEX `UNIQ_232B318C5EFFFFFE`, ADD UNIQUE `UNIQ_232B318C5EFFFFFE` (`game_id`, `color_id`) USING BTREE;
            '
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
