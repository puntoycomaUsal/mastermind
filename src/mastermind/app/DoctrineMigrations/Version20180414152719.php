<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180414152719 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql(
            '
                CREATE TABLE `color` (
                `id` int(11) NOT NULL,
                `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                `codeHex` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                `responseColor` tinyint(1) NOT NULL DEFAULT 0
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

              CREATE TABLE `config` (
                `id` int(11) NOT NULL,
                `totalPosition` int(11) NOT NULL,
                `totalPlay` int(11) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

              CREATE TABLE `game` (
                `id` int(11) NOT NULL,
                `status_id` int(11) DEFAULT 1,
                `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                `totalColors` int(11) NOT NULL,
                `totalPay` int(11) NOT NULL,
                `created` datetime NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

              CREATE TABLE `game_color` (
                `id` int(11) NOT NULL,
                `position` int(11) NOT NULL,
                `game_id` int(11) NOT NULL,
                `color_id` int(11) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

              CREATE TABLE `play` (
                `id` int(11) NOT NULL,
                `color_id` int(11) NOT NULL,
                `orderplay` int(11) NOT NULL,
                `created` datetime NOT NULL,
                `position` int(11) NOT NULL,
                `game_id` int(11) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

              CREATE TABLE `play_color` (
                `id` int(11) NOT NULL,
                `position` int(11) NOT NULL,
                `game_id` int(11) NOT NULL,
                `color_id` int(11) NOT NULL,
                `orderplay` int(11) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

              CREATE TABLE `status` (
                `id` int(11) NOT NULL,
                `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


              ALTER TABLE `color`
                ADD PRIMARY KEY (`id`),
                ADD UNIQUE KEY `UNIQ_665648E95E237E06` (`name`),
                ADD UNIQUE KEY `UNIQ_665648E9860758A1` (`codeHex`);

              ALTER TABLE `config`
                ADD PRIMARY KEY (`id`);

              ALTER TABLE `game`
                ADD PRIMARY KEY (`id`),
                ADD UNIQUE KEY `UNIQ_232B318C5E237E06` (`name`),
                ADD KEY `IDX_232B318C6BF700BD` (`status_id`);

              ALTER TABLE `game_color`
                ADD PRIMARY KEY (`id`),
                ADD UNIQUE KEY `UNIQ_232B318C5EFFFFFF`  (`position`,`game_id`,`color_id`),
                ADD KEY `IDX_54653166E48FD905` (`game_id`),
                ADD KEY `IDX_546531667ADA1FB5` (`color_id`);

              ALTER TABLE `play`
                ADD PRIMARY KEY (`id`),
                ADD KEY `IDX_5E89DEBA7ADA1FB5` (`color_id`),
                ADD KEY `game_id` (`game_id`),
                ADD KEY `orderplay` (`orderplay`),
                ADD KEY `position` (`position`);

              ALTER TABLE `play_color`
                ADD PRIMARY KEY (`id`),
                ADD UNIQUE KEY `UNIQ_232B318C5EFFFFFE`  (`position`,`game_id`,`color_id`),
                ADD KEY `IDX_9789AF55E48FD905` (`game_id`),
                ADD KEY `IDX_9789AF557ADA1FB5` (`color_id`);

              ALTER TABLE `status`
                ADD PRIMARY KEY (`id`),
                ADD UNIQUE KEY `UNIQ_7B00651C5E237E06` (`name`);

              ALTER TABLE `color`
                MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

              ALTER TABLE `config`
                MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

              ALTER TABLE `game`
                MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

              ALTER TABLE `play`
                MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

              ALTER TABLE `status`
                MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

              ALTER TABLE `game`
                ADD CONSTRAINT `FK_232B318C6BF700BD` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);

              ALTER TABLE `game_color`
                ADD CONSTRAINT `FK_546531667ADA1FB5` FOREIGN KEY (`color_id`) REFERENCES `color` (`id`),
                ADD CONSTRAINT `FK_54653166E48FD905` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`);

              ALTER TABLE `play`
                ADD CONSTRAINT `play_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
                ADD CONSTRAINT `play_ibfk_2` FOREIGN KEY (`color_id`) REFERENCES `color` (`id`);

              ALTER TABLE `play_color`
                ADD CONSTRAINT `FK_9789AF557ADA1FB5` FOREIGN KEY (`color_id`) REFERENCES `color` (`id`),
                ADD CONSTRAINT `FK_9789AF55E48FD905` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`);
            '
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
