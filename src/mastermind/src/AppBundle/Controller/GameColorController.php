<?php

namespace AppBundle\Controller;

use AppBundle\Entity\GameColor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Gamecolor controller.
 *
 * @Route("gamecolor")
 */
class GameColorController extends Controller
{
    /**
     * Lists all gameColor entities.
     *
     * @Route("/", name="gamecolor_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $gameColors = $em->getRepository('AppBundle:GameColor')->findAll();

        return $this->render('gamecolor/index.html.twig', array(
            'gameColors' => $gameColors,
        ));
    }

    /**
     * Creates a new gameColor entity.
     *
     * @Route("/new", name="gamecolor_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $gameColor = new Gamecolor();
        $form = $this->createForm('AppBundle\Form\GameColorType', $gameColor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($gameColor);
            $em->flush();

            return $this->redirectToRoute('gamecolor_show', array('id' => $gameColor->getId()));
        }

        return $this->render('gamecolor/new.html.twig', array(
            'gameColor' => $gameColor,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a gameColor entity.
     *
     * @Route("/{id}", name="gamecolor_show")
     * @Method("GET")
     */
    public function showAction(GameColor $gameColor)
    {
        $deleteForm = $this->createDeleteForm($gameColor);

        return $this->render('gamecolor/show.html.twig', array(
            'gameColor' => $gameColor,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing gameColor entity.
     *
     * @Route("/{id}/edit", name="gamecolor_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, GameColor $gameColor)
    {
        $deleteForm = $this->createDeleteForm($gameColor);
        $editForm = $this->createForm('AppBundle\Form\GameColorType', $gameColor);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gamecolor_edit', array('id' => $gameColor->getId()));
        }

        return $this->render('gamecolor/edit.html.twig', array(
            'gameColor' => $gameColor,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a gameColor entity.
     *
     * @Route("/{id}", name="gamecolor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, GameColor $gameColor)
    {
        $form = $this->createDeleteForm($gameColor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($gameColor);
            $em->flush();
        }

        return $this->redirectToRoute('gamecolor_index');
    }

    /**
     * Creates a form to delete a gameColor entity.
     *
     * @param GameColor $gameColor The gameColor entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(GameColor $gameColor)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gamecolor_delete', array('id' => $gameColor->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Comprobar que los colores que ingrese el usuario se correspondan con los colores de la partida.
     * El algortimo consistirá en una revisión en 2 pasos, en el primer paso, enfrentaremos $colors
     * con $secretColors, en caso de que coincida el id en posición y valor, sumaremos una unidad
     * al contador de colores negros, y cambiaremos en ambos arrays el id por una marca ("#") para
     * la segunda pasada del algoritmo. 
     * 
     * En la segunda pasada, recorreremos el array $colors de nuevo, y solo revisaremos los elementos
     * que son distintos de nuestra marca, comprobaremos si están incluidos en el array $secretColors
     * solo si están incluidos, si el id lo está, aumentaremos en 1 unidad el contador de colores
     * blancos, sino está incluido aumentaremos el contador de nulos en una unidad. 
     * 
     * Cuando se termine la segunda pasada, ya tendremos en los 3 contadores la respuesta, para 
     * generar un array de respuesta donde primero escribiremos el id del color negro tantas veces
     * como aparezca en el contador, segundo el de blanco y tercero el de valores nulos. 
     *
     * @param Array $colors Un listado de colores a evaluar
     * @param integer $gameId El id del juego al que hace referencia
     *
     * @return Array Un listado con las respuestas de la comparación.
     */
    public function checkGameColors($colors, $gameId)
    {
        //Declaración de variables necesarias
        $em = $this->getDoctrine()->getManager();
        $colorResponse  = array();
        $secretColors   = array();
        $blackColorId   = $em->getRepository('AppBundle:Color')->findOneBy([ 'blackcolor' => true])->getId();
        $whiteColorId   = $em->getRepository('AppBundle:Color')->findOneBy([ 'whitecolor' => true])->getId();
        $nullColorId    = $em->getRepository('AppBundle:Color')->findOneBy([ 'nullcolor'  => true])->getId();
        $game_colors    = $em->getRepository('AppBundle:GameColor')->findBy([ 'game' => $gameId], ['position' => 'ASC']);
        $countBlack     = 0;
        $countWhite     = 0;
        $countNull      = 0;
        $mark           = "#";

        //Se crea un array que contiene los colores de la combinación secreta
        foreach ($game_colors as $game_color){
            array_push ($secretColors,$game_color->getColor()->getId());
        }

        //Primera pasada del algoritmo, detección de pins negros, (colores correctos en posición y valor)
        foreach($colors as $key => $color){
            if (intval($color) == intval($secretColors[$key])){
                //El color de la jugada es igual en posición y valor al de la clave secreta
                $countBlack ++;
                //Marcamos en ambos arrays para que no computen en el cálculo de blancos.
                $colors[$key]          = $mark;
                $secretColors[$key]    = $mark;
            }
        }

        //Segunda pasada del algoritmo, detección de pins blancos, (colores correctos solo en valor)
        foreach($colors as $color){
            if ($color != $mark){
                if (in_array(intval($color),$secretColors)){
                    //Si está entre los colores pendientes, aumentamos el contador de pins blancos
                    $countWhite ++;
                } else{
                    //Sino aumentamos el contador de color nulo a devolver.
                    $countNull  ++;
                }
            } 
        }

        //Ahora los contadores tienen el número de cada respuesta que se debe marcar como respuesta
        for ($i = 1; $i <= $countBlack; $i++) {
            array_push($colorResponse,$blackColorId);
        }
        for ($i = 1; $i <= $countWhite; $i++) {
            array_push($colorResponse,$whiteColorId);
        }
        for ($i = 1; $i <= $countNull; $i++) {
            array_push($colorResponse,$nullColorId);
        }

        return $colorResponse;

    }

}
