<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PlayColor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Playcolor controller.
 *
 * @Route("playcolor")
 */
class PlayColorController extends Controller
{
    /**
     * Lists all playColor entities.
     *
     * @Route("/", name="playcolor_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $playColors = $em->getRepository('AppBundle:PlayColor')->findAll();

        return $this->render('playcolor/index.html.twig', array(
            'playColors' => $playColors,
        ));
    }

    /**
     * Creates a new playColor entity.
     *
     * @Route("/new", name="playcolor_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $playColor = new Playcolor();
        $form = $this->createForm('AppBundle\Form\PlayColorType', $playColor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($playColor);
            $em->flush();

            return $this->redirectToRoute('playcolor_show', array('id' => $playColor->getId()));
        }

        return $this->render('playcolor/new.html.twig', array(
            'playColor' => $playColor,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a playColor entity.
     *
     * @Route("/{id}", name="playcolor_show")
     * @Method("GET")
     */
    public function showAction(PlayColor $playColor)
    {
        $deleteForm = $this->createDeleteForm($playColor);

        return $this->render('playcolor/show.html.twig', array(
            'playColor' => $playColor,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing playColor entity.
     *
     * @Route("/{id}/edit", name="playcolor_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PlayColor $playColor)
    {
        $deleteForm = $this->createDeleteForm($playColor);
        $editForm = $this->createForm('AppBundle\Form\PlayColorType', $playColor);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('playcolor_edit', array('id' => $playColor->getId()));
        }

        return $this->render('playcolor/edit.html.twig', array(
            'playColor' => $playColor,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a playColor entity.
     *
     * @Route("/{id}", name="playcolor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PlayColor $playColor)
    {
        $form = $this->createDeleteForm($playColor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($playColor);
            $em->flush();
        }

        return $this->redirectToRoute('playcolor_index');
    }

    /**
     * Creates a form to delete a playColor entity.
     *
     * @param PlayColor $playColor The playColor entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PlayColor $playColor)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('playcolor_delete', array('id' => $playColor->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
