<?php

namespace AppBundle\Controller;

//Librerías
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;;
use FOS\RestBundle\View\View;
use Swagger\Annotations as SWG;

//Entidades
use AppBundle\Entity\Color;
use AppBundle\Entity\Game;
use AppBundle\Entity\Status;
use AppBundle\Entity\GameColor;
use AppBundle\Entity\Play;
use AppBundle\Entity\PlayColor;

//Controlaodres
use AppBundle\Controller\ColorController;
use AppBundle\Controller\GameColorController;

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="localhost:8000",
 *     basePath="/api/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Mastermind APi",
 *         description="Es una pequeña APiREST generada para ejercicio de APIS del título propio de desarrollador fullstack USAL",
 *         @SWG\Contact(
 *             email="administrador@apimastermindfullstackusal.es"
 *         ),
 *         @SWG\License(
 *             name="Apache 2.0",
 *             url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Si quieres saber como funciona Swagger, tienes más documentación aquí",
 *         url="http://swagger.io"
 *     )
 * )
 */
class ApiController extends FOSRestController
{
    /**
     * Muestra todos los colores que están disponibles para jugar a mastermind
     * 
     * @Rest\Get("/api/v1/color")
     * @Method("GET")
     * 
     * @SWG\Get(
     *     path="/color",
     *     tags={"color"},
     *     summary="Muestra todos los colores que están disponibles para jugar a mastermind",
     *     description="Muestra todos los colores que están disponibles para jugar y cuales de ellos marcarán respuestas",
     *     operationId="getColors",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Color")
     *         )
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="NOT FOUND",
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="INTERNAL SERVER ERROR",
     *         @SWG\Schema(type="string"),
     *     ),
     * )
     */
    public function listColors()
    {   
        try{
            $restresult = $this->getDoctrine()->getRepository('AppBundle:Color')->findAll();
            if ($restresult === null) {
                return new View("No hay colores en la base de datos", Response::HTTP_NOT_FOUND);
            }
            return $restresult;
        }catch(\Exception $e){
            return new View($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Muestra todos los estados que están disponibles para una partida en mastermind
     * 
     * @Rest\Get("/api/v1/status")
     * @Method("GET")
     * 
     * @SWG\Get(
     *     path="/status",
     *     tags={"status"},
     *     summary="Muestra todos los estados que están disponibles para una partida en mastermind",
     *     description="Muestra todos los estados que están disponibles para una partida en mastermind",
     *     operationId="getStatus",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Status")
     *         )
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="NOT FOUND",
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="INTERNAL SERVER ERROR",
     *         @SWG\Schema(type="string"),
     *     ),
     * )
     */
    public function listStatus()
    {
        try{
            $restresult = $this->getDoctrine()->getRepository('AppBundle:Status')->findAll();
            if ($restresult === null) {
                return new View("No hay estados en la base de datos", Response::HTTP_NOT_FOUND);
            }
            return $restresult;
        } catch(\Exception $e){
            return new View($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Listado de todos los juegos
     * 
     * @Rest\Get("/api/v1/game")
     * @Method("GET")
     * 
     * @SWG\Get(
     *     path="/game",
     *     tags={"game"},
     *     summary="Listado de todos los juegos",
     *     description="Listado de todos los juegos",
     *     operationId="getGame",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Game")
     *         )
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="NOT FOUND",
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="INTERNAL SERVER ERROR",
     *         @SWG\Schema(type="string"),
     *     ),
     * )
     */
    public function listGames()
    {
        try{
            $restresult = $this->getDoctrine()->getRepository('AppBundle:Game')->findAll();
            
            if ($restresult === null) {
                return new View("No hay partidas en la base de datos", Response::HTTP_NOT_FOUND);
            }

            return $restresult;

        } catch(\Exception $e){
            return new View($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Listado de juegos, se pueden consultar tanto todos los juegos como filtrar por
     * sus estados.
     * 
     * @Rest\Get("/api/v1/game/{status_id}")
     * @Method("GET")
     * 
     * @SWG\Get(
     *     path="/game/{status_id}",
     *     tags={"game"},
     *     summary="Listado de juegos, se pueden consultar tanto todos los juegos como filtrar por sus estados",
     *     description="Listado de juegos, se pueden consultar tanto todos los juegos como filtrar por sus estados",
     *     operationId="getGameStatus",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="status_id",
     *          in="path",
     *          description="Id del estado por el que queremos filtrar los juegos, sino lo enviamos, nos devolverá todos los juegos",
     *          type="integer",
     *          required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Game")
     *         )
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="NOT FOUND",
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="INTERNAL SERVER ERROR",
     *         @SWG\Schema(type="string"),
     *     ),
     * )
     */
    public function listGamesStatus($status_id)
    {
        try{
            $restresult = $this->getDoctrine()->getRepository('AppBundle:Game')->findBy(
                ['status' => $status_id],
                ['name' => 'ASC']
            );

            if ($restresult === null) {
                return new View("No hay partidas en la base de datos", Response::HTTP_NOT_FOUND);
            }

            return $restresult;

        } catch(\Exception $e){
            return new View($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Muestra una partida con todas sus jugadas.
     * 
     * @Rest\Get("/api/v1/game/play/{gameName}")
     * @Method("GET")
     * 
     *  @SWG\Get(
     *     path="/game/play/{gameName}",
     *     tags={"game"},
     *     summary="Muestra una partida con todas sus jugadas",
     *     description="Muestra una partida con todas sus jugadas",
     *     operationId="getGamePlayStatus",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="gameName",
     *          in="path",
     *          description="Id del estado por el que queremos filtrar los juegos, sino lo enviamos, nos devolverá todos los juegos",
     *          type="string",
     *          required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(type="object",
     *                        @SWG\Property(property="game", ref="#/definitions/Game"),
     *                        @SWG\Property(property="play", ref="#/definitions/Play"),
     *                        @SWG\Property(property="playcolor", ref="#/definitions/PlayColor"),
     *                        @SWG\Property(property="gamecolor", ref="#/definitions/GameColor"),
     *                      )
     *         )
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="NOT FOUND",
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="INTERNAL SERVER ERROR",
     *         @SWG\Schema(type="string"),
     *     ),
     * )
     */
    public function listGamesWithPlays($gameName)
    {
        try{
            $restresult = array();

            $game   = $this->getDoctrine()->getRepository('AppBundle:Game')->findOneBy(
                                                                            ['name' => $gameName]
                                                                        );
            if ($game === null){
                return new View("No hay partidas en la base de datos", Response::HTTP_NOT_FOUND);
            }

            $restresult["game"] = $game;
            
            $plays  = $this->getDoctrine()->getRepository('AppBundle:Play')->findBy(
                                                                            ['game' => $game],
                                                                            ['orderplay' => 'ASC']
                                                                            );
            
            if (!is_null($plays)){
                $restresult["play"] = $plays;
            }

            $playcolors = $this->getDoctrine()->getRepository('AppBundle:PlayColor')->findBy(
                                                                            ['game' => $game],
                                                                            ['orderplay' => 'ASC']
                                                                            );

            if (!is_null($playcolors)){
                $restresult["playcolor"] = $playcolors;
            }

            //Solamente si la partida está finalizada se devuelve la combinación
            if ($game->getStatus()->getId() > 1){
                $gamecolors = $this->getDoctrine()->getRepository('AppBundle:GameColor')->findBy(
                                                                            ['game' => $game]
                                                                            );

                if (!is_null($gamecolors)){
                    $restresult["gamecolor"] = $gamecolors;
                }
            }

            return $restresult;
        } catch(\Exception $e){
            return new View($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Crear una partida nueva
     * 
     * @Rest\Post("/api/v1/game")
     * @Method("POST")
     * 
     * @SWG\Post(
     *     path="/game",
     *     tags={"game"},
     *     summary="Crear una partida nueva",
     *     description="Crear una partida nueva",
     *     operationId="postGame",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="name",
     *          in="formData",
     *          description="Nombre para crear una nueva partida, se puede dejar vacío",
     *          type="string",
     *          required=false,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Game")
     *         )
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="NOT FOUND",
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="INTERNAL SERVER ERROR",
     *         @SWG\Schema(type="string"),
     *     ),
     * )
     */
    public function createGame(Request $request)
    {
        try{
            //Parámetros POST
            $name   = "game_";
            if (!is_null($request->request->get("name"))) {
                $name = $request->request->get("name");
            }

            //Traemos los datos maestros necesarios
            $entityManager  = $this->getDoctrine()->getManager();
            $config         = $entityManager->getRepository('AppBundle:Config')->find(1);
            $status         = $entityManager->getRepository('AppBundle:Status')->find(1);

            //Creamos el nuevo registro
            $game = new Game();
            $game->setName($name);
            $game->setTotalColors($config->getTotalposition());
            $game->setTotalplay($config->getTotalplay());
            $game->setCreated(new \DateTime());
            $game->setStatus($status);

            $entityManager->persist($game);
            $entityManager->flush();

            if (is_null($request->request->get("name"))){
                //Actualizamos el registro de nombre con su clave
                //realmente este código debería ejecutarse en un
                //trigger en la base de datos, en la tabla Game
                //en el evento disparado después de grabar el
                //registro.
                $game->setName($game->getName().strval($game->getId()));
                $entityManager->flush();
            }

            //Generación del código de colores.
            $position   = 0;
            foreach (ColorController::randomColors($game->getTotalColors()) as $randomColor){
                //randomColor contiene el id de un color
                $position ++;
                $color      = $entityManager->getRepository('AppBundle:Color')->find($randomColor);

                $gamecolor  = new GameColor();
                $gamecolor->setPosition($position);
                $gamecolor->setGame($game);
                $gamecolor->setColor($color);

                $entityManager->persist($gamecolor);
                $entityManager->flush();
            }

            return $game;

            }    catch(\Exception $e){
                return new View($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
    }


    /**
     * Crear una jugada para una partida nueva
     * 
     * @Rest\Post("/api/v1/play")
     * @Method("POST")
     * 
     * @SWG\Post(
     *     path="/play",
     *     tags={"play"},
     *     summary="Crear una jugada para una partida nueva",
     *     description="Crear una jugada para una partida nueva",
     *     operationId="postPlay",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="nameGame",
     *          in="formData",
     *          description="Nombre de la partida a la que hace referencia la jugada",
     *          type="string",
     *          required=true,
     *     ),
     *     @SWG\Parameter(
     *          name="colors[]",
     *          in="formData",
     *          description="Array de enteros con los ids de los colores que forman la partida",
     *          type="array",
     *          @SWG\Items(type="integer"),
     *          collectionFormat="multi",
     *          uniqueItems=true,
     *          required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(type="object",
     *                        @SWG\Property(property="status", ref="#/definitions/Status"),
     *                        @SWG\Property(property="responseColors", type="array",@SWG\Items(type="integer"))
     *                      )
     *         )
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="NOT FOUND",
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="INTERNAL SERVER ERROR",
     *         @SWG\Schema(type="string"),
     *     ),
     * )
     */
    public function createPlay(Request $request)
    {
        try {
            //Parámetros POST

            //Comprobar que los parámetros no estan vacíos.
            if (is_null($request->request->get("nameGame"))) {
                return new View("Indica el nombre de la partida.", Response::HTTP_BAD_REQUEST);
            }

            if (is_null($request->request->get("colors"))) {
                return new View("Indica los colores de la jugada", Response::HTTP_BAD_REQUEST);
            }

            $colors         = $request->request->get("colors");
            $nameGame       = $request->request->get("nameGame");

            //Variables
            $entityManager  = $this->getDoctrine()->getManager();
            $blackColorId   = $entityManager->getRepository('AppBundle:Color')->findOneBy([ 'blackcolor' => true])->getId();
            $countBlack     = 0;
            $response       = array();

            //Comprueba si la partida existe y esta en juego
            $game =  $entityManager->getRepository('AppBundle:Game')->findOneBy(
                                                                ['name'         => $nameGame,
                                                                'status'       => 1]
                                                            );       

            if (is_null($game)) {
                return new View("El nombre de partida indicado no existe o no está en juego en este momento", Response::HTTP_NOT_FOUND);
            } 

            //Comprobación tamaño del array colors correcto
            if (count($colors) != $game->getTotalColors()) {
                return new View("El número de colores que has enviado debe ser " . $game->getTotalColors() . " y es " . count($colors) . ".", Response::HTTP_BAD_REQUEST);   
            }

            //Comprobación jugadas pendientes
            $lastPlay   =  $entityManager->getRepository('AppBundle:Play')->findOneBy(
                                                                ['game' => $game],
                                                                ['orderplay' => 'DESC']
                                                            );
            //Calculamos el orden de la jugada
            if (is_null($lastPlay)) {
                //Si no hay jugadas previas (es la primera)
                $orderPlay = 1;
            } else {
                $orderPlay = $lastPlay->getOrderplay()+1;
            }
            
            if ($orderPlay > $game->getTotalplay()) {
                //Este control es redundante, ya que si todo está bien programado, la partida debería tener
                //estado de finalizada con victoria o derrota, al hacer la comprobación de colores.
                return new View("La partida no dispone de más jugadas.", Response::HTTP_BAD_REQUEST);
            }

            //Añadimos un nuevo control, para revisar los colores antes de empezar a operar con ellos.
            foreach ($colors as $key => $idColor) {
                //Traemos el color, y comprobamos que en efecto existen
                $color = $entityManager->getRepository('AppBundle:Color')->find(intval($idColor));
                if (is_null($color)){
                    return new View("El código de color " . $idColor . " enviado en la posición ". $key." no existe", Response::HTTP_BAD_REQUEST);
                }
            }

            //Consultaros y generamos el array de respuesta, para generar el registro en playcolors.
            $responseColors  =  GameColorController::checkGameColors($colors, $game->getId());

            //Almacenamos la jugada
            foreach ($colors as $key => $idColor) {

                //Traemos el color ya revisado sabiendo que existe.
                $color = $entityManager->getRepository('AppBundle:Color')->find(intval($idColor));

                $play = new Play();
                $play->setColor($color);
                $play->setOrderplay($orderPlay);
                $play->setCreated(new \DateTime());
                $play->setPosition($key);
                $play->setGame($game);

                $entityManager->persist($play);
                $entityManager->flush();
            }
            
            //Almacenamos y analizamos la respuesta
            foreach ($responseColors as $key => $idResponseColor) {
                //Si es un pin negro, contabilizamos
                if (intval($idResponseColor) == intval($blackColorId)){
                    $countBlack ++;
                }

                //Traemos el color
                $color = $entityManager->getRepository('AppBundle:Color')->find(intval($idResponseColor));

                $playcolor  = new PlayColor();
                $playcolor->setPosition($key);
                $playcolor->setGame($game);
                $playcolor->setColor($color);
                $playcolor->setOrderplay($orderPlay);

                $entityManager->persist($playcolor);
                $entityManager->flush();

            }

            //Comprobamos posibles casos de finalización.
            if ($orderPlay == $game->getTotalplay()){
                //Es la última jugada
                if ($countBlack == $game->getTotalColors()){
                    //Si hay tantos pines negros como colores por jugada, finaliza con derrota para el servidor
                    $newStatus = $entityManager->getRepository('AppBundle:Status')->find(3);
                } else {
                    //Si hay menos pines negros como colores por jugada, finaliza con victoria para el servidor
                    $newStatus = $entityManager->getRepository('AppBundle:Status')->find(2);
                }
            } else {
                //Aún quedan más jugadas
                if ($countBlack == $game->getTotalColors()){
                    //Si hay tantos pines negros como colores por jugada, finaliza con derrota para el servidor
                    $newStatus = $entityManager->getRepository('AppBundle:Status')->find(3);
                }
            }

            //Si hay nuevo estado, se graba el nuevo estado en la cabecera del juego.
            if (isset($newStatus)){
                $game->setStatus($newStatus);
                $entityManager->flush();
            }

            //Devolvemos un array que contendrá el estado de la partida, y la respuesta de colores
            $response["status"]           = $game->getStatus();
            $response["responseColors"]   = $responseColors;

            return $response;
        } catch(\Exception $e){
            return new View($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    } 
}
