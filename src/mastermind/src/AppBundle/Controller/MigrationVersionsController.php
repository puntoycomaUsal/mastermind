<?php

namespace AppBundle\Controller;

use AppBundle\Entity\MigrationVersions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Migrationversion controller.
 *
 * @Route("migrationversions")
 */
class MigrationVersionsController extends Controller
{
    /**
     * Lists all migrationVersion entities.
     *
     * @Route("/", name="migrationversions_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $migrationVersions = $em->getRepository('AppBundle:MigrationVersions')->findAll();

        return $this->render('migrationversions/index.html.twig', array(
            'migrationVersions' => $migrationVersions,
        ));
    }

    /**
     * Creates a new migrationVersion entity.
     *
     * @Route("/new", name="migrationversions_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $migrationVersion = new Migrationversion();
        $form = $this->createForm('AppBundle\Form\MigrationVersionsType', $migrationVersion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($migrationVersion);
            $em->flush();

            return $this->redirectToRoute('migrationversions_show', array('version' => $migrationVersion->getVersion()));
        }

        return $this->render('migrationversions/new.html.twig', array(
            'migrationVersion' => $migrationVersion,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a migrationVersion entity.
     *
     * @Route("/{version}", name="migrationversions_show")
     * @Method("GET")
     */
    public function showAction(MigrationVersions $migrationVersion)
    {
        $deleteForm = $this->createDeleteForm($migrationVersion);

        return $this->render('migrationversions/show.html.twig', array(
            'migrationVersion' => $migrationVersion,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing migrationVersion entity.
     *
     * @Route("/{version}/edit", name="migrationversions_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, MigrationVersions $migrationVersion)
    {
        $deleteForm = $this->createDeleteForm($migrationVersion);
        $editForm = $this->createForm('AppBundle\Form\MigrationVersionsType', $migrationVersion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('migrationversions_edit', array('version' => $migrationVersion->getVersion()));
        }

        return $this->render('migrationversions/edit.html.twig', array(
            'migrationVersion' => $migrationVersion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a migrationVersion entity.
     *
     * @Route("/{version}", name="migrationversions_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, MigrationVersions $migrationVersion)
    {
        $form = $this->createDeleteForm($migrationVersion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($migrationVersion);
            $em->flush();
        }

        return $this->redirectToRoute('migrationversions_index');
    }

    /**
     * Creates a form to delete a migrationVersion entity.
     *
     * @param MigrationVersions $migrationVersion The migrationVersion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MigrationVersions $migrationVersion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('migrationversions_delete', array('version' => $migrationVersion->getVersion())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
