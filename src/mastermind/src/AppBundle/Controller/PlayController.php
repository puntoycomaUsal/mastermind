<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Play;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Play controller.
 *
 * @Route("play")
 */
class PlayController extends Controller
{
    /**
     * Lists all play entities.
     *
     * @Route("/", name="play_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $plays = $em->getRepository('AppBundle:Play')->findAll();

        return $this->render('play/index.html.twig', array(
            'plays' => $plays,
        ));
    }

    /**
     * Creates a new play entity.
     *
     * @Route("/new", name="play_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $play = new Play();
        $form = $this->createForm('AppBundle\Form\PlayType', $play);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($play);
            $em->flush();

            return $this->redirectToRoute('play_show', array('id' => $play->getId()));
        }

        return $this->render('play/new.html.twig', array(
            'play' => $play,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a play entity.
     *
     * @Route("/{id}", name="play_show")
     * @Method("GET")
     */
    public function showAction(Play $play)
    {
        $deleteForm = $this->createDeleteForm($play);

        return $this->render('play/show.html.twig', array(
            'play' => $play,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing play entity.
     *
     * @Route("/{id}/edit", name="play_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Play $play)
    {
        $deleteForm = $this->createDeleteForm($play);
        $editForm = $this->createForm('AppBundle\Form\PlayType', $play);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_edit', array('id' => $play->getId()));
        }

        return $this->render('play/edit.html.twig', array(
            'play' => $play,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a play entity.
     *
     * @Route("/{id}", name="play_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Play $play)
    {
        $form = $this->createDeleteForm($play);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($play);
            $em->flush();
        }

        return $this->redirectToRoute('play_index');
    }

    /**
     * Creates a form to delete a play entity.
     *
     * @param Play $play The play entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Play $play)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('play_delete', array('id' => $play->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
