<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

/**
 * Play
 *
 * @ORM\Table(name="play", indexes={@ORM\Index(name="IDX_5E89DEBA7ADA1FB5", columns={"color_id"}), @ORM\Index(name="game_id", columns={"game_id"}), @ORM\Index(name="orderplay", columns={"orderplay"}), @ORM\Index(name="position", columns={"position"})})
 * @ORM\Entity
 * @SWG\Definition()
 */
class Play
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @SWG\Property(description="Identificador de jugada")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="orderplay", type="integer", nullable=false)
     * @SWG\Property(description="Número de jugada dentro de la partida")
     */
    private $orderplay;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @SWG\Property(description="Fecha de la jugada")
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     * @SWG\Property(description="Posición del color en el orden de respuesta dentro de la jugada")
     */
    private $position;

    /**
     * @var \Game
     *
     * @ORM\ManyToOne(targetEntity="Game")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     * })
     * @SWG\Property(description="Juego al que pertenece la jugada")
     */
    private $game;

    /**
     * @var \Color
     *
     * @ORM\ManyToOne(targetEntity="Color")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     * })
     * @SWG\Property(description="Color de la posición en la jugada")
     */
    private $color;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderplay
     *
     * @param integer $orderplay
     *
     * @return Play
     */
    public function setOrderplay($orderplay)
    {
        $this->orderplay = $orderplay;

        return $this;
    }

    /**
     * Get orderplay
     *
     * @return integer
     */
    public function getOrderplay()
    {
        return $this->orderplay;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Play
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Play
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set game
     *
     * @param \AppBundle\Entity\Game $game
     *
     * @return Play
     */
    public function setGame(\AppBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \AppBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set color
     *
     * @param \AppBundle\Entity\Color $color
     *
     * @return Play
     */
    public function setColor(\AppBundle\Entity\Color $color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return \AppBundle\Entity\Color
     */
    public function getColor()
    {
        return $this->color;
    }
}
