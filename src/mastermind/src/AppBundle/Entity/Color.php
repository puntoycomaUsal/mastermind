<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

/**
 * Color
 *
 * @ORM\Table(name="color", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_665648E95E237E06", columns={"name"}), @ORM\UniqueConstraint(name="UNIQ_665648E9860758A1", columns={"codeHex"})})
 * @ORM\Entity
 * @SWG\Definition()
 */
class Color
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @SWG\Property(description="Identificador de color")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @SWG\Property(description="Nombre de color")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="codeHex", type="string", length=255, nullable=false)
     * @SWG\Property(description="Código hexadecimal de color")
     */
    private $codehex;

    /**
     * @var boolean
     *
     * @ORM\Column(name="blackColor", type="boolean", nullable=false)
     * @SWG\Property(description="Indica si este color será el color para devolver respuestas acertadas en posición y color")
     */
    private $blackcolor = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="whiteColor", type="boolean", nullable=false)
     * @SWG\Property(description="Indica si este color será el color para devolver respuestas acertadas en color")
     */
    private $whitecolor = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="nullColor", type="boolean", nullable=false)
     * @SWG\Property(description="Indica si este color será el color para devolver erróneas acertadas en posición y color")
     */
    private $nullcolor = '0';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Color
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set codehex
     *
     * @param string $codehex
     *
     * @return Color
     */
    public function setCodehex($codehex)
    {
        $this->codehex = $codehex;

        return $this;
    }

    /**
     * Get codehex
     *
     * @return string
     */
    public function getCodehex()
    {
        return $this->codehex;
    }

    /**
     * Set blackcolor
     *
     * @param boolean $blackcolor
     *
     * @return Color
     */
    public function setBlackcolor($blackcolor)
    {
        $this->blackcolor = $blackcolor;

        return $this;
    }

    /**
     * Get blackcolor
     *
     * @return boolean
     */
    public function getBlackcolor()
    {
        return $this->blackcolor;
    }

    /**
     * Set whitecolor
     *
     * @param boolean $whitecolor
     *
     * @return Color
     */
    public function setWhitecolor($whitecolor)
    {
        $this->whitecolor = $whitecolor;

        return $this;
    }

    /**
     * Get whitecolor
     *
     * @return boolean
     */
    public function getWhitecolor()
    {
        return $this->whitecolor;
    }

    /**
     * Set nullcolor
     *
     * @param boolean $nullcolor
     *
     * @return Color
     */
    public function setNullcolor($nullcolor)
    {
        $this->nullcolor = $nullcolor;

        return $this;
    }

    /**
     * Get nullcolor
     *
     * @return boolean
     */
    public function getNullcolor()
    {
        return $this->nullcolor;
    }
}
