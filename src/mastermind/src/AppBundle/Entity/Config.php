<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Config
 *
 * @ORM\Table(name="config")
 * @ORM\Entity
 */
class Config
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="totalPosition", type="integer", nullable=false)
     */
    private $totalposition;

    /**
     * @var integer
     *
     * @ORM\Column(name="totalPlay", type="integer", nullable=false)
     */
    private $totalplay;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set totalposition
     *
     * @param integer $totalposition
     *
     * @return Config
     */
    public function setTotalposition($totalposition)
    {
        $this->totalposition = $totalposition;

        return $this;
    }

    /**
     * Get totalposition
     *
     * @return integer
     */
    public function getTotalposition()
    {
        return $this->totalposition;
    }

    /**
     * Set totalplay
     *
     * @param integer $totalplay
     *
     * @return Config
     */
    public function setTotalplay($totalplay)
    {
        $this->totalplay = $totalplay;

        return $this;
    }

    /**
     * Get totalplay
     *
     * @return integer
     */
    public function getTotalplay()
    {
        return $this->totalplay;
    }
}
