<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

/**
 * Game
 *
 * @ORM\Table(name="game", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_232B318C5E237E06", columns={"name"})}, indexes={@ORM\Index(name="IDX_232B318C6BF700BD", columns={"status_id"})})
 * @ORM\Entity
 * @SWG\Definition()
 */
class Game
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @SWG\Property(description="Identificador de partida")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @SWG\Property(description="Nombre de partida")
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="totalColors", type="integer", nullable=false)
     * @SWG\Property(description="Número total de colores por jugada y en la clave del servidor")
     */
    private $totalcolors;

    /**
     * @var integer
     *
     * @ORM\Column(name="totalPlay", type="integer", nullable=false)
     * @SWG\Property(description="Número máximo de jugadas por partida")
     */
    private $totalplay;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @SWG\Property(description="Fecha de inicio de la partida")
     */
    private $created;

    /**
     * @var \Status
     *
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     * })
     * @SWG\Property(description="Estado de la partida")
     */
    private $status;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Game
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set totalcolors
     *
     * @param integer $totalcolors
     *
     * @return Game
     */
    public function setTotalcolors($totalcolors)
    {
        $this->totalcolors = $totalcolors;

        return $this;
    }

    /**
     * Get totalcolors
     *
     * @return integer
     */
    public function getTotalcolors()
    {
        return $this->totalcolors;
    }

    /**
     * Set totalplay
     *
     * @param integer $totalplay
     *
     * @return Game
     */
    public function setTotalplay($totalplay)
    {
        $this->totalplay = $totalplay;

        return $this;
    }

    /**
     * Get totalplay
     *
     * @return integer
     */
    public function getTotalplay()
    {
        return $this->totalplay;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Game
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\Status $status
     *
     * @return Game
     */
    public function setStatus(\AppBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }
}
