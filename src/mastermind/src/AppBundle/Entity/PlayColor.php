<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

/**
 * PlayColor
 *
 * @ORM\Table(name="play_color", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_232B318C5EFFFFFE", columns={"position", "game_id", "color_id"})}, indexes={@ORM\Index(name="IDX_9789AF55E48FD905", columns={"game_id"}), @ORM\Index(name="IDX_9789AF557ADA1FB5", columns={"color_id"})})
 * @ORM\Entity
 * @SWG\Definition()
 */
class PlayColor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @SWG\Property(description="Identificador de la respuesta del servidor a la jugada")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     * @SWG\Property(description="Posición de la respuesta en la jugada")
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="orderplay", type="integer", nullable=false)
     * @SWG\Property(description="Orden de jugada dentro de la partida")
     */
    private $orderplay;

    /**
     * @var \Color
     *
     * @ORM\ManyToOne(targetEntity="Color")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     * })
     * @SWG\Property(description="Color de la respuesta a la jugada en la posición")
     */
    private $color;

    /**
     * @var \Game
     *
     * @ORM\ManyToOne(targetEntity="Game")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     * })
     * @SWG\Property(description="Partida de la jugada")
     */
    private $game;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return PlayColor
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set orderplay
     *
     * @param integer $orderplay
     *
     * @return PlayColor
     */
    public function setOrderplay($orderplay)
    {
        $this->orderplay = $orderplay;

        return $this;
    }

    /**
     * Get orderplay
     *
     * @return integer
     */
    public function getOrderplay()
    {
        return $this->orderplay;
    }

    /**
     * Set color
     *
     * @param \AppBundle\Entity\Color $color
     *
     * @return PlayColor
     */
    public function setColor(\AppBundle\Entity\Color $color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return \AppBundle\Entity\Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set game
     *
     * @param \AppBundle\Entity\Game $game
     *
     * @return PlayColor
     */
    public function setGame(\AppBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \AppBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }
}
